package com.trian.model;

import java.sql.Blob;

public class Image {
	
	// Attributes of empImage class
	private int imgId;
	private Blob empImage;
	private String path;

	//getters and setters for image path
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	//getters and setters for blob image
	public Blob getEmpImage() {
		return empImage;
	}
	public void setEmpImage(Blob image) {
		this.empImage = image;
	}
	public int getImgId() {
		return imgId;
	}
	public void setImgId(int imgId) {
		this.imgId = imgId;
	}
}

