package com.trian.model;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {
	
	public void mail(String to, String subject,Multipart multipart, String message, final String user, final String password ) throws AddressException, MessagingException {
		//input Server Details 
		Properties pro = new Properties();		
		
		pro.put("mail.smtp.host", "smtp.gmail.com");
		pro.put("mail.smtp.port", "587");
		pro.put("mail.smtp.auth", "true");
		pro.put("mail.smtp.starttls.enable", "true");
		
		Session session = Session.getInstance(pro, new javax.mail.Authenticator(){	
			protected PasswordAuthentication getPasswordAuthentication(){
			return new PasswordAuthentication(user, password);
			}
		} );	
		//Set the mail details
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(user));
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		msg.setSubject(subject);
		msg.setText(message);
        msg.setContent(multipart);
		Transport.send(msg);
	}
	
	

}
