package com.trian.model;

public class Employee {	

	//attributes of employee class
	private int empID;
	private String name;
	private String designation;
	private String birthday;
	private String email;
	
	
	public Employee() {
		
	}
	
	
	public Employee(int empID, String name, String designation, String birthday, String email) {
		super();
		this.empID = empID;
		this.name = name;
		this.designation = designation;
		this.birthday = birthday;
		this.email = email;
	}
	/*
	 * Getters and setters for the above attribute
	 * 
	 */
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birth) {
		this.birthday = birth;
	}
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
	
}
