package com.trian.bo.impl;


import com.trian.bd.EmployeeBo;
import com.trian.dao.EmployeeDao;
import com.trian.model.Employee;
import com.trian.model.Image;

public class EmployeeBoImpl implements EmployeeBo {
	
	EmployeeDao employeeDao;

	public EmployeeDao getEmployeeDao() {
		return employeeDao;
	}

	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	@Override
	public void addEmployee(Employee emp) {
		getEmployeeDao().addEmployee(emp);
	}

	@Override
	public void addImage(Image img) {
		
		getEmployeeDao().addImage(img);
	}
	public void run() {
		getEmployeeDao().run();
	}
}
