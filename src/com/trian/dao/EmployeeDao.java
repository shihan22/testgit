package com.trian.dao;



import com.trian.model.Employee;
import com.trian.model.Image;

public interface EmployeeDao {
	public void addEmployee(Employee emp);
	public void addImage(Image img);
	public void run();
	
}
