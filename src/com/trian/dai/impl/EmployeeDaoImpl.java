package com.trian.dai.impl;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.*;


import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.trian.dao.EmployeeDao;
import com.trian.model.Employee;
import com.trian.model.Image;
import com.trian.model.Mail;

public class EmployeeDaoImpl extends HibernateDaoSupport implements EmployeeDao {

	public void addEmployee(Employee emp) {
	
		getHibernateTemplate().save(emp);
	}
	
	public void addImage(Image img) {
		getHibernateTemplate().save(img);
		
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void run() {
		// get current date
		LocalDate localDate = LocalDate.now();//For reference
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd");
		String formattedString = localDate.format(formatter);
		
		//create session
		
		AnnotationConfiguration config1 = new AnnotationConfiguration();
		config1.addAnnotatedClass(Employee.class);
	
		SessionFactory factory= config1.configure().buildSessionFactory();
		Session session1 = factory.getCurrentSession();
		session1.beginTransaction();
				
		//get the all employees as a list
	
		List<Employee> result1 = (List<Employee>)session1.createQuery("from Employee").list();
		//get all the images as a list 
	
		List<Image> imgs = (List<Image>)session1.createQuery("from Image").list(); 
		
		//get the system time
		java.util.Date date = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a");

		//time format 00:00 AM || PM 
		
		//Ssystem time in above format
		String myTime = sdf.format(date);
		//mail sending time
		final String allocateTime = "10:00 AM";
		System.out.println(myTime);
		
		// MAKE REFERENCE FROM MODEL CLASSES		
		Image img = null;
		Employee emp = null; 
		
		//loop for get the each and every employers birth data
		for (int i = 0; i < result1.size(); i++) {	
			
			if( result1.size() > imgs.size() || result1.size() == 0){
					int tot = result1.size() - imgs.size();
					JOptionPane.showMessageDialog (null, "Add " +  tot + " more Images !!" , "Warning", JOptionPane.INFORMATION_MESSAGE);		
					System.exit(0);
			} 

				emp = (Employee) result1.get(i);
				img = (Image)imgs.get(i);
				
				//attach the image
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				Multipart multipart = new MimeMultipart();
				messageBodyPart = new MimeBodyPart();
				String file = img.getPath();
				String fileName = "Birthday";
				DataSource source = new FileDataSource(file);
				try {
					messageBodyPart.setDataHandler(new DataHandler(source));
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
				try {
					messageBodyPart.setFileName(fileName);
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
				try {
					multipart.addBodyPart(messageBodyPart);
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
				
				//get the birtdate to a string
				String empDate = emp.getBirthday();
					
				//for check the execution
				System.out.println("Executed !");
					        
				//check date and month of the birthday
				if(formattedString.substring(0, 5).equalsIgnoreCase(empDate.substring(0, 5) ) ){
					if(myTime.equalsIgnoreCase(allocateTime)){				
							Mail m = new Mail();
							try {
								//send mail
								m.mail(emp.getEmail(), "Wish You A Very Happy Birthday ..!!", multipart, "Message", "testmenuwee@gmail.com", "Test@123");
							} catch (MessagingException e) {
								
								e.printStackTrace();
					}
				}
			}	
		}			
	}	

	
	
}
