package com.trian.action;
import com.trian.model.Image;
import com.trian.bd.EmployeeBo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.struts.ActionSupport;



@SuppressWarnings("deprecation")
public class AddImageAction extends ActionSupport {

		public ActionForward execute(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) 
	        throws Exception {
	 
			EmployeeBo employeeBo =
				(EmployeeBo) getWebApplicationContext().getBean("employeeBo");
			
			ImageForm imageForm = (ImageForm)form;
			Image image = new Image();
			
			//copy customer form to model
			BeanUtils.copyProperties(image, imageForm);
			
			//save it
			employeeBo.addImage(image);
		        
			return mapping.findForward("success");
		  
		}
		
}
	 