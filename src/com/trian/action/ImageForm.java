package com.trian.action;

import java.sql.Blob;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

@SuppressWarnings("serial")
public class ImageForm extends ActionForm {
	private int imgId;
	private Blob empImage;
	private String path;

	public int getImgId() {
		return imgId;
	}
	public void setImgId(int imgId) {
		this.imgId = imgId;
	}

	public Blob getEmpImage() {
		return empImage;
	}
	public void setEmpImage(Blob empImage) {
		this.empImage = empImage;
	}

	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,
	HttpServletRequest request) {
 
	    ActionErrors errors = new ActionErrors();
 
	    if( getImgId() == 0 || ("".equals(getImgId()))) {
	       errors.add("err",
              new ActionMessage("err"));
	    }
	    
	    if( getEmpImage() == null ) {
		   errors.add("err",
	           new ActionMessage("err"));
		}
	    
	    if( getPath() == null || ("".equals(getPath()))) {
			   errors.add("err",
		           new ActionMessage("err"));
		}
	

	    return errors;
	}
 
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// reset properties
		imgId = 0;
		empImage = null;
		path = "";
	}
}
