package com.trian.action;
import com.trian.bd.EmployeeBo;
import com.trian.model.Employee;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.struts.ActionSupport;

@SuppressWarnings("deprecation")
public class AddEmployeeAction extends ActionSupport {

		public ActionForward execute(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) 
	        throws Exception {
	 
			EmployeeBo employeeBo =
				(EmployeeBo) getWebApplicationContext().getBean("employeeBo");
			
			EmployeeForm employeeForm = (EmployeeForm)form;
			Employee employee = new Employee();
			
			//copy customer form to model
			BeanUtils.copyProperties(employee, employeeForm);
			
			//save it
			employeeBo.addEmployee(employee);
		        
			return mapping.findForward("success");
		  
		}
	 
	
}
