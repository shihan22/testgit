package com.trian.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

@SuppressWarnings("serial")
public class EmployeeForm extends ActionForm {
	
	private String name;
	private String designation;
	private String birthday;
	private String email;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

	@Override
	public ActionErrors validate(ActionMapping mapping,
	HttpServletRequest request) {
	    ActionErrors errors = new ActionErrors();
 
	    if( getName() == null || ("".equals(getName()))) {
	    	
	       errors.add("err",
              new ActionMessage("err"));
	    }
	    
	    if( getBirthday() == null || ("".equals(getBirthday()))) {
		   errors.add("err",
	           new ActionMessage("err"));
		}
	    
	    if( getDesignation() == null || ("".equals(getDesignation()))) {
			   errors.add("err",
		           new ActionMessage("err"));
		}
	    
	    if( getEmail() == null || ("".equals(getEmail()))) {
			   errors.add("err",
		           new ActionMessage("err"));
		}

	    return errors;
	}
 
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// reset properties
		name = "";
		email = "";
		birthday = "";
		designation = "";
	
	}
	
	
	
	
	
}
