package com.trian.bd;



import com.trian.model.Employee;
import com.trian.model.Image;

public interface EmployeeBo {
	
	public void addEmployee(Employee emp);
	public void addImage(Image img);
	public void run();
	
}
