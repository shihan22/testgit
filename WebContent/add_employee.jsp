<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
<head>
</head>
<body>
<h1 align="center">Employee</h1>


<div style="color:red">
	<html:errors/>
</div>

<html:form action="/AddEmployee" >
<div style="padding:16px">
	<div style="float:left;width:100px;">
		<bean:message key="employee.label.name"  /> :
	</div>
 
	<html:text property="name" size="40" maxlength="20"/>
</div>

<div style="padding:16px">
	<div style="float:left;width:100px;">
		<bean:message key="employee.label.birthday" /> : 
	</div> 
 
	<html:text property="birthday" size="40" maxlength="20"/>
</div>

<div style="padding:16px">
	<div style="float:left;width:100px;">
		<bean:message key="employee.label.designation" /> : 
	</div> 
 
	<html:text property="designation" size="40" maxlength="20"/>
</div>

<div style="padding:16px">
	<div style="float:left;width:100px;">
		<bean:message key="employee.label.email" /> : 
	</div> 
 
	<html:text property="email" size="40" maxlength="20"/>
</div>

 
<div style="padding:16px">
	<div style="float:left;padding-right:8px;">
		<html:submit>
             <bean:message key="employee.label.submit" />
        </html:submit>
	</div>
	<html:reset>
          <bean:message key="employee.label.reset" />
     </html:reset>
</div>
 
</html:form>
 
</body>
</html>